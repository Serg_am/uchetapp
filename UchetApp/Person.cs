﻿    using System;
namespace UchetApp
{
    public class Person
    {
        public string Name { get; set;}
        public string SecondName { get; set; }
        public string Sex { get; set; }
        public string Age { get; set; }
        public string Department { get; set; }
        public int ID { get; set; }

        public override string ToString()
        {
            return "ID: " + ID + " Имя: " + Name + " Фамилия: " + SecondName +
                   " Пол: " + Sex + " Возраст: " + Age + " Отдел: " + Department;
        }

    }
}
