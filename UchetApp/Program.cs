﻿using System;
using System.Collections.Generic;

namespace UchetApp
{
    class Program
    {
        static void Main(string[] args)
        {
            string login = "admin",
                   password = "admin",
                   command;
            int i = 0;

            Console.Write("Введите логин:");

            while (Console.ReadLine() != login)
            {
                Console.WriteLine("Неверный логин, попробуйте еще раз\nП.С. ;) Логин:admin Пароль:admin");
                Console.Write("Введите логин:");
            }

            Console.Write("Введите пароль:");

            if (Console.ReadLine() != password)
            {
                Environment.Exit(0);
            }

            Console.WriteLine("Следующие команды:\n" +
                "add - добавить сотрдника\n" +
                "list - посмотреть список сотрудников\n" + 
                "remove[id] - удаляет сотрудника\n" +
                "exit - выход");

            List<Person> listP = new List<Person>();

            while ((command = Console.ReadLine()) != "exit")
            {
                if (command == "add")
                {
                    Console.WriteLine("Введите по очереди: Имя, Фамилия, Пол, Возраст, Отдел");
                    listP.Add(new Person()
                    {
                        Name = Console.ReadLine(),
                        ID = i,
                        SecondName = Console.ReadLine(),
                        Sex = Console.ReadLine(),
                        Age = Console.ReadLine(),
                        Department = Console.ReadLine()
                    });

                    i++;
                    Console.WriteLine("Введите команду");
                }

                else if (command == "list")
                {
                    foreach (Person aList in listP)
                    {
                        Console.WriteLine(aList);
                    }
                    Console.WriteLine("Введите команду");
                }

                else if(command == "remove") // Тут пока еще не додумался как через ИД удалить
                {
                    listP.RemoveAt(i);
                }

                else
                    Console.WriteLine("Введите корректную команду");
            }

        }
    }
}
